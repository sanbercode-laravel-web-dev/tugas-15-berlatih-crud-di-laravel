<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome-page', [AuthController::class, 'welcome_page']);

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/table', function(){
    return view('pages.table');
});
Route::get('/data-table', function(){
    return view('pages.data-table');
});

// CRUD Cast
// Create Form Add Cast
Route::get('/cast/create', [CastController::class, 'create']);
// Send Data to database
Route::post('/cast', [CastController::class, 'store']);
// Read
Route::get('/cast', [CastController::class, 'index']);
// Read Details
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
// Update
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
// Delete
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);