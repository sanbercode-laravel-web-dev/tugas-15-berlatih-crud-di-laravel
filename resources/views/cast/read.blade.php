@extends('layout.master')

@section('title')
List Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-success btn-sm mb-2">add</a>
<table class="table table-bordered">
    <thead>
        <tr>
            <th style="width: 10px">#</th>
            <th>Name</th>
            <th>Age</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->name}}</td>
                <td>{{$value->age}}</td>
                <td>
                    <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm mr-1">Details</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm mr-1">Update</a>
                    <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
                
        @empty
            <tr>
                <td></td>
                <td>Data not found</td>
                <td></td>
                <td></td>
            </tr>
        @endforelse
        
    </tbody>
</table>
@endsection
