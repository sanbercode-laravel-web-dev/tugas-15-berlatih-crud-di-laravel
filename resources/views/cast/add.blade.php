@extends('layout.master')

@section('title')
Add Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="mb-3">
        <label class="form-label">Nama</label>
        <input type="text" name="name" class="form-control">
    </div>
    @error('name')
        <div class="alert alert-danger mb-1" role="alert">{{ $message }}</div>
    @enderror
    <div class="mb-3">
        <label class="form-label">Umur</label>
        <input type="text" name="age" class="form-control">
    </div>
    @error('age')
        <div class="alert alert-danger mb-1" role="alert">{{ $message }}</div>
    @enderror
    <div class="mb-3">
        <label class="form-label">Bio</label>
        <textarea name="bio" id="" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger mb-1" role="alert">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
